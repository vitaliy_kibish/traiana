export default [
  {
    id: "0",
    title: "LogIn",
    required: [
      "login",
      "password"
    ],
    fields: [
      {
        name: "login",
        type: "text",
        label: "Username"
      },
      {
        name: "password",
        type: "password",
        label: "Password"
      },
      {
        name: "remember",
        type: "checkbox",
        label: "Remember Me"
      }
    ],
    submit: {
      url: '/api/...',
      value: "Log In"
    }
  },
  {
    id: "1",
    title: "SignUp",
    required: [
      "firstName",
      "lastName",
      "email",
      "password"
    ],
    fields: [
      {
        name: "firstName",
        type: "text",
        label: "First Name"
      },
      {
        name: "lastName",
        type: "text",
        label: "Last Name"
      },
      {
        name: "email",
        type: "email",
        label: "Email"
      },
      {
        name: "password",
        type: "password",
        label: "Password"
      },
    ],
    submit: {
      url: '/api/...',
      value: "Sign Up",
    }
  },
  {
    id: "2",
    title: "Contact Us",
    required: [
      "fullName",
      "email",
      "textarea"
    ],
    fields: [
      {
        name: "fullName",
        type: "text",
        label: "Full Name"
      },
      {
        name: "email",
        type: "text",
        label: "Email"
      },
      {
        name: "phone",
        type: "text",
        label: "Phone"
      },
      {
        name: "textarea",
        type: "textarea",
        label: "Discription"
      },
    ],
    submit: {
      url: '/api/...',
      value: "Contact Us"
    }
  }
];
