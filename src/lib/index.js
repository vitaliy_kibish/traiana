import data from '../data';

export const fakeFetch = () => Promise.resolve(data);
