import React, { Component } from 'react';
import { fakeFetch } from '../../lib';

// styles
import './styles/app.scss';

// components
import FormBuilder from '../FormBuilder';
import Modal from '../Modal';
import Button from '../FormElements/Button';
import Label from '../FormElements/Label';
import Radio from '../FormElements/Radio';

import { EmptyConfigMsg } from './message';

class App extends Component {
  constructor (props) {
    super(props);
    this.chooseForm = this.chooseForm.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);

    this.state = {
      forms: [],
      showModal: false,
      current: 0,
    };
  }

  componentDidMount() {
    fakeFetch()
      .then(forms => this.setState({ forms }));
  }

  chooseForm(current) {
    this.setState({ current: Number(current) });
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { forms, current, showModal } = this.state;

    const formLabels = forms.map(({ id, title }, i) => {
      const checked = current === i;

      return (
        <li key={id}>
          <Label>
            { title }
            <Radio
              value={i}
              checked={checked}
              onChange={this.chooseForm} />
          </Label>
        </li>
      );
    });

    const currentForm = forms[current];
    const modal = showModal && currentForm && (
      <Modal hideModal={this.hideModal}>
        <FormBuilder form={currentForm} />
      </Modal>
    );

    if (!formLabels.length) {
      return (
        <div>
          <div className="app-container">
            <EmptyConfigMsg />
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="app-container">
          <h2 className="app-title">
            Choose your form
          </h2>
          <ul>
            { formLabels  }
          </ul>
          <Button onClick={this.showModal}>
            Create
          </Button>
        </div>
        { modal }
      </div>
    );
  }
}

export default App;
