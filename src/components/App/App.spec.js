import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';

import App from './index.js';

describe("<App/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<App />);

    expect(wrapper.find('.app-container').length).to.equal(1);
  });

  it('the componentDidMount should be invoked once', () => {
    sinon.spy(App.prototype, 'componentDidMount');
    const wrapper = mount(<App />);

    expect(App.prototype.componentDidMount.calledOnce).to.equal(true);
  });

  it('should return actual state', () => {
    const wrapper = shallow(<App />);

    expect(wrapper.state('current')).to.equal(0);
  });

  it('.chooseForm() should return a new state', () => {
    const wrapper = shallow(<App />);

    wrapper.instance().chooseForm(2);
    expect(wrapper.state().current).to.equal(2);
  });

  it('.showModal() should return a new state', () => {
    const wrapper = shallow(<App />);

    wrapper.instance().showModal();
    expect(wrapper.state().showModal).to.equal(true);
  });

  it('.hideModal() should return a new state', () => {
    const wrapper = shallow(<App />);

    wrapper.instance().hideModal();
    expect(wrapper.state().showModal).to.equal(false);
  });
});
