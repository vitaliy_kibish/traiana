import React from 'react';

// styles
import './styles/message.scss';

export const EmptyConfigMsg = () => (
  <p className="message">Your form-config is empty :(</p>
);
