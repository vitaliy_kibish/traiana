import React, { Component } from 'react';

// styles
import './styles/modal.scss';

// components
import Button from '../FormElements/Button';

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { hideModal } = this.props;

    return (
      <div className="modal-bg" onClick={hideModal}>
        <div className="modal-container" onClick={(e) => e.stopPropagation()}>
          <button className="modal-close" onClick={hideModal}>✕</button>
          { this.props.children }
        </div>
      </div>
    );
  }
}

export default Modal;
