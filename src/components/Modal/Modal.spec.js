import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';

import Modal from './index.js';

describe("<Modal/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Modal />);
    expect(wrapper.find('.modal-container').length).to.equal(1);
  });

  it('should have props for hideModal', () => {
    const wrapper = shallow(<Modal hideModal={x => x} />);
    expect(wrapper.props().onClick).to.be.a('function');
  });

  it('should handle .onClick()', () => {
    const handleClick = sinon.spy();
    const wrapper = shallow(<Modal hideModal={handleClick} />);
    wrapper.find('button').simulate('click');
    expect(handleClick.calledOnce).to.equal(true);
  });
});
