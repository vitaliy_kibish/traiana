import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';

import FormBuilder from './index.js';
import Input from '../FormElements/Input';
import data from '../../data';

describe("<FormBuilder/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<FormBuilder form={data[0]} />);

    expect(wrapper.find('form').length).to.equal(1);
  });

  it('should return actual props', () => {
    const wrapper = shallow(<FormBuilder form={data[0]} />);

    expect(wrapper.instance().props).to.have.property('form');
  });

  it('.handleChange() should return new state', () => {
    const value = 'qwerty';
    const name = 'password';
    const wrapper = shallow(<FormBuilder form={data[0]} />);

    wrapper.instance().handleChange(value, name);
    expect(wrapper.state()[name]).to.equal(value);
  });

  it('.renderFormField() should return <Input /> component', () => {
    const type = 'text';
    const name = 'name';
    const value = 'value';
    const req = true;

    const wrapper = shallow(<FormBuilder form={data[0]} />);
    const TestInput = wrapper.instance().renderFormField(type, name, value, req);

    expect(TestInput.type).to.equal(Input);
  });
});
