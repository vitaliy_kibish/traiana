import React, { Component, PropTypes } from 'react';

// styles
import './styles/formBuilder.scss';

// components
import Form from '../FormElements/Form';
import Input from '../FormElements/Input';
import Label from '../FormElements/Label';
import Button from '../FormElements/Button';
import CheckBox from '../FormElements/CheckBox';
import Radio from '../FormElements/Radio';
import TextArea from '../FormElements/TextArea';

class FormBuilder extends Component {
  static get propTypes() {
    return {
      form: PropTypes.shape({
        required: PropTypes.arrayOf(PropTypes.string),
        title: PropTypes.string.isRequired,
        fields: PropTypes.arrayOf(
          PropTypes.shape({
            name: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
          })
        ).isRequired,
        submit: PropTypes.shape({
          url: PropTypes.string.isRequired,
          value: PropTypes.string.isRequired,
        }).isRequired,
      }).isRequired,
    }
  }

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.renderFormField = this.renderFormField.bind(this);

    const { fields } = props.form;

    const form = fields.reduce((result, field) => {
      result[field.name] = '';

      return result;
    }, {});

    this.state = form;
  }

  handleChange(value, name) {
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state);
  }

  renderFormField(type, name, value, req) {
    switch (type) {
      case 'checkbox':
        return <CheckBox
          id={name}
          checked={Boolean(this.state[name])}
          name={name}
          onChange={this.handleChange} />;
      case 'radio':
        return <Radio
          id={name}
          value={value}
          checked={value === this.state[name]}
          name={name}
          onChange={this.handleChange} />;
      case 'textarea':
        return <TextArea
          id={name}
          required={req}
          value={this.state[name]}
          name={name}
          onChange={this.handleChange} />;
      default:
        return <Input
          id={name}
          type={type}
          required={req}
          value={this.state[name]}
          name={name}
          onChange={this.handleChange} />;;
    }
  }

  render() {
    const { form: { title, fields, submit, required } } = this.props;

    const fieldElements = fields.map(({ name, type, label, value }, i) => {
      const req = required.indexOf(name) !== -1;

      return (
        <div className="label-container" key={i}>
          <Label htmlFor={name}>
            {label}
          </Label>
          {this.renderFormField(type, name, value, req)}
        </div>
      );
    });

    return (
      <Form onSubmit={this.handleSubmit}>
        <h3 className="form-builder-title">{title}</h3>
        { fieldElements }
        <div className="form-builder-container center">
          <Button type="submit">
            { submit.value }
          </Button>
        </div>
      </Form>
    );
  }
}

export default FormBuilder;
