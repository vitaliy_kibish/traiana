import React from 'react';
import { expect } from 'chai';
import { render } from 'enzyme';

import Form from './index.js';

describe("<Form/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Form onSubmit={x => x} />);
    
    expect(wrapper.find('form').length).to.equal(1);
  });
});
