import React, { PropTypes } from 'react';

// styles
import './styles/input.scss';

const propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

const Input = ({ name, required, type, value, onChange }) => {
  const handleChange = (e) => {
    const { value, name } = e.target;

    onChange(value, name);
  }

  return (
    <input
      className="input"
      required={required}
      id={name}
      type={type}
      name={name}
      value={value}
      onChange={handleChange} />
  );
};

Input.propTypes = propTypes;

export default Input;
