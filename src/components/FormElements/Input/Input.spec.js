import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import sinon from 'sinon';

import Input from './index.js';

describe("<Input/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Input name="input" type="input" onChange={x => x} />);
    expect(wrapper.find('.input').length).to.equal(1);
  });

  it('should handle .onChange()', () => {
    const handleChange = sinon.spy();
    const wrapper = shallow(<Input name="input" type="input" onChange={handleChange} />);
    wrapper.find('input').simulate('change', { target: {} });
    expect(handleChange.calledOnce).to.equal(true);
  });
});
