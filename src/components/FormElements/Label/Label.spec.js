import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import sinon from 'sinon';

import Label from './index.js';

describe("<Label/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Label />);
    expect(wrapper.find('.label').length).to.equal(1);
  });
});
