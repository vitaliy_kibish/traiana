import React, { PropTypes } from 'react';

// styles
import './styles/checkBox.scss';

const propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

const CheckBox = ({ name, checked, onChange }) => {
  const handleChange = (e) => {
    const { checked, name } = e.target;

    onChange(checked, name);
  }

  return (
    <input
      className="checkbox"
      type="checkbox"
      id={name}
      name={name}
      checked={checked}
      onChange={handleChange} />
  );
};

CheckBox.propTypes = propTypes;

export default CheckBox;
