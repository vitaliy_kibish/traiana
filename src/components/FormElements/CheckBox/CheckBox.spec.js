import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';

import CheckBox from './index.js';

describe("<CheckBox/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<CheckBox name="check" checked={true} onChange={x => x}/>);
    expect(wrapper.find('.checkbox').length).to.equal(1);
  });

  it('should handle .onChange()', () => {
    const handleChange = sinon.spy();
    const wrapper = shallow(<CheckBox name="check" checked={false} onChange={handleChange} />);
    wrapper.find('input').simulate('change', { target: {} });
    expect(handleChange.calledOnce).to.equal(true);
  });
});
