import React, { PropTypes } from 'react';

// styles
import './styles/radio.scss';

const propTypes = {
  checked: PropTypes.bool.isRequired,
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired,
  ]),
  onChange: PropTypes.func.isRequired,
};

const Radio = ({ name, checked, value, onChange }) => {
  const handleChange = (e) => {
    const { value, name } = e.target;

    onChange(value, name);
  }

  return (
    <input
      className="radio"
      type="radio"
      id={name}
      value={value}
      name={name}
      checked={checked}
      onChange={handleChange} />
  );
};

Radio.propTypes = propTypes;

export default Radio;
