import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import sinon from 'sinon';

import Radio from './index.js';

describe("<Radio/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Radio checked={true} onChange={x => x} />);

    expect(wrapper.find('.radio').length).to.equal(1);
  });

  it('should handle .onChange()', () => {
    const handleChange = sinon.spy();
    const wrapper = shallow(<Radio checked={true} onChange={handleChange}/>);

    wrapper.find('input').simulate('change', { target: {} });
    expect(handleChange.calledOnce).to.equal(true);
  });
});
