import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';

import Button from './index.js';

describe("<Button/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<Button />);
    expect(wrapper.find('.button').length).to.equal(1);
  });

  it('should handle .onClick()', () => {
    const handleClick = sinon.spy();
    const wrapper = shallow(<Button onClick={handleClick} />);
    wrapper.find('button').simulate('click');
    expect(handleClick.calledOnce).to.equal(true);
  });
});
