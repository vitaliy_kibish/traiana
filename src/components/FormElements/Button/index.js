import React, { PropTypes } from 'react';

// styles
import './styles/button.scss';

const propTypes = {
  type: PropTypes.string,
  children: PropTypes.any,
  onClick: PropTypes.func,
};

const Button = ({ type, children, onClick }) => (
  <button className="button" type={type} onClick={onClick}>
    { children }
  </button>
);

Button.propTypes = propTypes;

export default Button;
