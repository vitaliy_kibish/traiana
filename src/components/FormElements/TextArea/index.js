import React, { PropTypes } from 'react';

// styles
import './styles/textArea.scss';

const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

const TextArea = ({ name, value, required, onChange }) => {
  const handleChange = (e) => {
    const { value, name } = e.target;

    onChange(value, name);
  }

  return (
    <textarea
      className="text-area"
      required={required}
      id={name}
      name={name}
      value={value}
      onChange={handleChange} />
  );
};

TextArea.propTypes = propTypes;

export default TextArea;
