import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import sinon from 'sinon';

import TextArea from './index.js';

describe("<TextArea/> component", function() {
  it('markup should contain selector', () => {
    const wrapper = render(<TextArea name="name" onChange={x => x} />);

    expect(wrapper.find('.text-area').length).to.equal(1);
  });

  it('should handle .onChange()', () => {
    const handleChange = sinon.spy();
    const wrapper = shallow(<TextArea name="name" onChange={handleChange} />);

    wrapper.find('textarea').simulate('change', { target: {} });
    expect(handleChange.calledOnce).to.equal(true);
  });
});
